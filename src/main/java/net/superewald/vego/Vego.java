package net.superewald.vego;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.minecraft.client.render.RenderLayer;
import net.superewald.vego.block.ModBlocks;
import net.superewald.vego.item.ModItems;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;	

public class Vego implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger("vego");

	@Override
	public void onInitialize() {
		ModBlocks.register();
		ModItems.register();

		BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.SOY_PLANT, RenderLayer.getCutout());
	}
}
