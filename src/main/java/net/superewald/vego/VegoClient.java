package net.superewald.vego;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.minecraft.client.render.RenderLayer;
import net.fabricmc.api.ClientModInitializer;

@Environment(EnvType.CLIENT)
public class VegoClient implements ClientModInitializer {
    
    @Override
    public void onInitializeClient() {
    }
}
