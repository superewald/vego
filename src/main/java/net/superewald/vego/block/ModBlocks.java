package net.superewald.vego.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.CropBlock;
import net.minecraft.block.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModBlocks {
    
	public static final CropBlock SOY_PLANT = registerBlock("soy_plant",
        new SoyPlantBlock(AbstractBlock.Settings.of(Material.PLANT).nonOpaque().noCollision().ticksRandomly().breakInstantly().sounds(BlockSoundGroup.CROP)),
        null);



    private static <T extends Block> T registerBlock(String name, T block, ItemGroup itemGroup) {
        Registry.register(Registry.BLOCK, new Identifier("vego", name), block);
        if (itemGroup != null) {
            Registry.register(Registry.ITEM,
                    new Identifier("vego", name),
                    new BlockItem(block, new Item.Settings().group(itemGroup)));
        }
        return block;
    }

    public static void register() {
        // load class
    }    
}
