package net.superewald.vego.item;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.BlockItem;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.superewald.vego.block.ModBlocks;

public class ModItems {
    public static final Item SOY_SEEDS = registerItem("soy_seeds", 
        new BlockItem(ModBlocks.SOY_PLANT, new FabricItemSettings().group(ItemGroup.MISC)));

    public static final Item SOY = registerItem("soy", 
        new Item(new FabricItemSettings().food(new FoodComponent.Builder().hunger(2).saturationModifier(0.2f).build()).group(ItemGroup.FOOD)));


    private static Item registerItem(String name, Item item) {
        return Registry.register(Registry.ITEM, new Identifier("vego", name), item);
    }

    public static void register() {

    }
}
